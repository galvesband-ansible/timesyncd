Timesyncd ansible role
=========

This is a simple ansible-role for configuring the server time sync daemon of systemd. 
I'm no expert in Ansible so use with care. The tasks in this role are so 
straighforward that they hardly justify a full role uploaded to internet. Also keep 
in mind that Ubuntu 20.04 default configuration is enough in most cases. It is more 
an exercise to define and test how I want to build future more complex roles.

Requirements
------------

This role assumes the server runs **Ubuntu 20.04**, as most of the machines I'm dealing 
with run that OS. Maybe in the future I'll add some support to more distributions, 
when the need arises.

Role Variables
--------------

The following variables can be defined for this role. Most of them are straight out of
[man](https://www.freedesktop.org/software/systemd/man/timesyncd.conf.html) 
documentation.

 - `timesyncd_timezone`: Server time zone. Defaults to `Etc/UTC`.

 - `timesyncd_servers`: List of server names or ips of ntp servers. Defaults to `[]`.

 - `timesyncd_fallback_servers`: List of server names or ips of ntp servers. 
   Defaults to the follwing list of pools:
   - `0.ubuntu.pool.ntp.org`
   - `1.ubuntu.pool.ntp.org`
   - `2.ubuntu.pool.ntp.org`
   - `3.ubuntu.pool.ntp.org`
   - `ntp.ubuntu.com`

 - `timesyncd_root_distance_max_seconds`: Maximum acceptable root distance (estimated time 
   required by a packet to travel between this host and an ntp server). If the current 
   server does not satisfy this limit, the system will switch to another server.
   Defaults to `5`.

 - `timesyncd_poll_interval_min_seconds`: Minimum poll interval for NTP messages.
   Defaults to `32`.

 - `timesyncd_poll_interval_max_seconds`: Maximum poll interval for NTP messages.
   Defaults to `2048`.


Dependencies
------------

None.

Example Playbook
----------------

### Installation ###

The recommended approach is to use a `requirements.yml` file in your setup, with
something like this:

```yml
---

- src: git+https://gitlab.com/galvesband-ansible/timesyncd.git
  version: v1.0.0
```

You should also set up a directory for installed roles in the setup. One common
way to do this is by editing or creating an `ansible.cfg` in the root of the 
setup and adding a `roles_path` key pointing to the desired directory. By 
default ansible will install roles somewhere like `~/.ansible/roles` or something.

After that, just run this:

```
$ ansible-galaxy install -r requirements.yml
```


### Playbook example ###

A simple example.

```yml
---

- hosts: somehosts
  gather_facts: yes
  become: yes
  roles:

   - name: timesyncd
     tags: ["common"]
```

An example with ntp pools defined in variables:
```yml
---

- hosts: somehosts
  gather_facts: yes
  become: yes
  role:

   - name: timesyncd
     vars:
       timesyncd_timezone: Europe/Madrid
       timesyncd_servers:
         - hora.cica.es
```


Testing
-------

Right now is broken. Molecule is set up and ready, but the driver
used is `docker`, which has problems when the role checks for service
status or tries to restart a service, etc. because systemd inside
docker is not meant to be.

Maybe at some point I'll set up vagrant testing... Or maybe this 
problem gets fixed (at some point, it looks like it worked, see
[this issue in systemd](https://github.com/systemd/systemd/issues/19245)).


License
-------

[GPL 2.0 Or later](https://spdx.org/licenses/GPL-2.0-or-later.html).



